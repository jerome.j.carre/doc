---
author: Pierre Smeyers
---

# self-managed _to be continuous_ (advanced)

Using the official _to be continuous_ templates, you may have some specific needs within your company:

* simply expose some of your mutualized tools configuration in [Kicker](/kicker) (ex: a shared SonarQube server, an Artifactory, a private Kubernetes cluster),
* or maybe develop and share some template adjustments to address your specific technical context (ex: no default untagged runner, need to access the internet through a proxy, ...),
* or even develop your own internal templates.

## Variable presets

Variable presets are groups of _to be continuous_ variable values that can be used within your company.

You can simply define variable presets:

1. Create a GitLab project (if possible with `public` or at least `internal` visibility),
2. Create a `kicker-extras.json` declaring your presets,<br/>
_JSON schema: [https://gitlab.com/to-be-continuous/kicker/raw/master/kicker-extras-schema-1.json](https://gitlab.com/to-be-continuous/kicker/raw/master/kicker-extras-schema-1.json)_
3. Make a first release by creating a Git tag.

Example `kicker-extras.json`:

```json
{
  "presets": [
    {
      "name": "Shared SonarQube",
      "description": "Our internal shared SonarQube server",
      "values": {
        "SONAR_HOST_URL": "https://sonarqube.acme.host"
      }
    },
    {
      "name": "Shared OpenShift",
      "description": "Our internal shared OpenShift clusters (prod & no-prod)",
      "values": {
        "OS_URL": "https://api.openshift-noprod.acme.host",
        "OS_ENVIRONMENT_URL": "https://%{environment_name}.apps-noprod.acme.host",
        "OS_PROD_URL": "https://api.openshift-prod.acme.host",
        "OS_PROD_ENVIRONMENT_URL": "https://%{environment_name}.apps.acme.host",
        "K8S_URL": "https://api.openshift-noprod.acme.host",
        "K8S_ENVIRONMENT_URL": "https://%{environment_name}.apps-noprod.acme.host",
        "K8S_PROD_URL": "https://api.openshift-prod.acme.host",
        "K8S_PROD_ENVIRONMENT_URL": "https://%{environment_name}.apps.acme.host"
      }
    },
    {
      "name": "Artifactory Mirrors",
      "description": "Our internal Artifactory mirrors",
      "values": {
        "NPM_CONFIG_REGISTRY": "https://artifactory.acme.host/api/npm/npm-mirror",
        "DOCKER_REGISTRY_MIRROR": "https://dockerproxy.acme.host",
        "GOPROXY": "https://artifactory.acme.host/api/go/go-mirror",
        "PIP_INDEX_URL": "https://artifactory.acme.host/api/pypi/pythonproxy/simple",
        "PYTHON_REPOSITORY_URL": "https://artifactory.acme.host/api/pypi/python-mirror"
      }
    }
  ]
}
```

With this, [Kicker](/kicker) will prompt about each applicable preset directly from the online form.

## Template variants

Another essential extra resource is **template variants**. Roughly speaking, this is a template override to address a specific technical issue.

You can simply define a template variant:

1. Create a GitLab project (if possible with `public` or at least `internal` visibility),
2. Create a `kicker-extras.json` declaring your variant,<br/>
_JSON schema: [https://gitlab.com/to-be-continuous/kicker/raw/master/kicker-extras-schema-1.json](https://gitlab.com/to-be-continuous/kicker/raw/master/kicker-extras-schema-1.json)_
3. Make a first release by creating a Git tag.

Example: let's imagine in your company - in addition to the default untagged shared runners - you would also like to let users use a non-default shared runner to deploy to your private Kubernetes cluster. Let's also suppose those runners don't have a free access to the internet, but need to go through an http proxy.

That would involve:

1. develop the following variant for the [Kubernetes template](https://gitlab.com/to-be-continuous/kubernetes).
For instance in file `templates/acme-k8s-variant.yml`:
  ```yaml
  # ==========================================
  # === ACME variant to use Kubernetes runners
  # ==========================================
  # override kubernetes base template job
  .k8s-base:
    # Kubernetes Runners tags
    tags:
      - k8s
      - shared
    # Kubernetes Runners proxy configuration
    variables:
      http_proxy: "http://proxy.acme.host:8080"
      https_proxy: "http://proxy.acme.host:8080"
      no_proxy: "localhost,127.0.0.1,.acme.host"
      HTTP_PROXY: "${http_proxy}"
      HTTPS_PROXY: "${https_proxy}"
      NO_PROXY: "${no_proxy}"
  ```
  _(developing this requires [advanced to be continuous knowledge](../usage.md#advanced-usage-override-yaml))_
2. declare it in a `kicker-extras.json` file:
  ```json
  {
    "variants": [
      {
        "id": "acme-k8s-runners",
        "name": "ACME Kubernetes Runners",
        "description": "Use the ACME Kubernetes shared Runners",
        "template_path": "templates/acme-k8s-variant.yml",
        "target_project": "to-be-continuous/kubernetes"
      }
    ]
  }
  ```
  _(the `target_project` field declares the original template the variant applies to)_

This way, your variant will show up as a simple _actionable_ component in the Kubernetes template form in [Kicker](/kicker).

## Develop your own templates

You may also have to develop tooling very specific to your company.

In that case, you just have to:

1. Create a GitLab project (if possible with `public` or at least `internal` visibility),
2. Develop your template following the [guidelines](../dev/guidelines.md),
3. Declare the template with a [Kicker descriptor](../dev/guidelines.md#kicker-descriptor),
4. Make a first release by creating a Git tag.

Your template can then be used like any other _to be continuous_ one.

## Have your own doc + kicker

If you developed any of the above (Kicker extras and/or internal templates), you'll want all
developers from your company to have an easy access to a reference documentation + Kicker with your additional material.

In your local copy of the [doc](https://gitlab.com/to-be-continuous/doc) project:

1. Declare the CI/CD project variable `GITLAB_TOKEN`: a [group access token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html) with scopes `api,read_registry,write_registry,read_repository,write_repository` and with `Owner` role.
2. Declare the CI/CD project variable `KICKER_RESOURCE_GROUPS`: JSON configuration of GitLab groups to crawl.
3. create a scheduled pipeline (for instance every day at 3:00 am).

Here is an example of `KICKER_RESOURCE_GROUPS` content:

```json
[
  {
    "path": "acme/cicd/all", 
    "visibility": "public"
  },
  {
    "path": "acme/cicd/ai-ml", 
    "visibility": "internal", 
    "exclude": ["project-2", "project-13"],
    "extension": 
      {
        "id": "ai-ml", 
        "name": "AI/ML", 
        "description": "ACME templates for AI/ML projects"
      }
  },
  {
    "path": "to-be-continuous", 
    "visibility": "public"
  }
]
```

Some explanations:

* `path` is a path to a [GitLab group](https://docs.gitlab.com/ee/user/group/)
  with [GitLab projects](https://docs.gitlab.com/ee/user/project/) containing Kicker resources.
* `visibility` is the group/projects visibility to crawl.
* `exclude` (optional) allows to exclude some project(s) from processing.
* `extension` (optional) allows to associate Kicker resources with a separate extension (actionable within Kicker).

By default, `KICKER_RESOURCE_GROUPS` is configured to crawl the `to-be-continuous` group only.

## Setup tracking

Another optional thing you might want to setup is **tracking** (to collect statistics about template jobs execution).

_to be continuous_ already provides an unconfigured project to perform this: [tools/tracking](https://gitlab.com/to-be-continuous/tools/tracking).

Here is what you'll have to do to set it up:

1. Install an [Elasticsearch](https://www.elastic.co/elasticsearch/) server.
2. Create a dedicated user with appropriate authorization to push data to some indices.
3. In your local copy of the [tools/tracking](https://gitlab.com/to-be-continuous/tools/tracking):
define the `TRACKING_CONFIGURATION` CI/CD project variable as follows:
    ```JSON
    {
      "clients": [
        {
          "url":"https://elasticsearch-host",
          "authentication": {
            "username":"tbc-tracking",
            "password":"mYp@55w0rd"
          },
          "timeout":5,
          "indexPrefix":"tbc-",
          "esMajorVersion":7,
          "skipSslVerification":true
        }
      ]
    }
    ```
4. Manually start a pipeline on the `main` (or `master`) branch: this will (re)generate a new Docker image with your
   configuration that will now be used by every template job.

## Use Docker registry mirrors

### _to be continuous_ uses explicit Docker registries

By default Docker images names that do not specify a registry (e.g. `alpine:latest`) are [fetched from the Docker Hub](https://docs.docker.com/engine/reference/commandline/pull/#pull-from-a-different-registry).

Since Docker Hub has some [quotas](https://docs.docker.com/docker-hub/download-rate-limit/), some companies use Docker registry mirrors.

Some Docker registry mirrors can mirror multiple registries (e.g. [Artifactory](https://www.jfrog.com/confluence/display/JFROG/Virtual+Repositories) or [Nexus Repository](https://help.sonatype.com/repomanager3) when coupling [Docker Proxy](https://help.sonatype.com/repomanager3/nexus-repository-administration/formats/docker-registry/proxy-repository-for-docker) and [Docker Group](https://help.sonatype.com/repomanager3/nexus-repository-administration/formats/docker-registry/grouping-docker-repositories)).

In that case, when pulling an image without specifying the original registry, the mirror will look for an image with the same name in each of the upstream registries.

It will return the 1st matching image, which is not necessarily from the registry you expected.

Example:

- a developer builds the `superapp/backend:1.0.0` image and pushes it to both Docker Hub and Quay.io
- the developer also tags this image with tag `latest` and pushes the `latest` tag to both Docker Hub and Quay.io
- the developer then builds image `superapp/backend:1.1.0` and pushes it only to Docker Hub (e.g. because of a failure in the build pipeline) without noticing that the image has not been pushed to Quay.io
- if a user pulls `superapp/backend:latest` he would expect to get the `superapp/backend:1.1.0` image from Docker Hub
- but if a mirror has been set up to proxy both Docker Hub and Quay.io with a priority to Quay.io, the returned image would be `superapp/backend:1.0.0` pulled from Quay.io

This behavior can be used by attackers in supply chain attacks: they can push a malicious image in lots of Docker registries with the same name as a trustworthy image which is only published in the Docker Hub. A mirror could return the malicious image just because it found an image with the correct name on a different registry before the Docker Hub.

In order to protect against this kind of attacks, _to-be-continuous_ always use fully qualified image names (i.e. including the registry).

Example:

To refer to `aquasec/trivy:latest`, _to be continuous_ templates will always specify `registry.hub.docker.com/aquasec/trivy:latest`


### Drawbacks

When using [`containerd`](https://containerd.io/) as a container runtime, this should have no impact, containerd will still try to use the configured Docker registry mirrors if any.

On the other hand, when using Docker as a container runtime, specifying the registry name when pulling a Docker image prevents Docker from using a Docker registry mirror. Instead, the Docker daemon will directly pull the image from the specified registry. As a consequence, Docker Hub quotas may be reached sooner.


### Workarounds

You can simply override the image names specifying your own Docker registry mirror.

Example:

If you have a Docker registry mirror for the Docker Hub, you can use something like this:

```yaml
variables:
  DOCKER_TRIVY_IMAGE: "docker-proxy.mycorp.org/aquasec/trivy:latest"
```

In this case, both `containerd` and the Docker daemon will try to pull the `aquasec/trivy:latest` image through your Docker registry mirror.

